# viratannotations

Repository for the public VIRAT video data annotations as annotated by the IARPA DIVA program.

Annotations are in the [KPF](https://gitlab.kitware.com/meva/meva-data-repo/-/blob/master/documents/KPF-specification-v4.pdf) format.
